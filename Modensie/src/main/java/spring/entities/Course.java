package spring.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="Course")
public class Course {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "course_id", columnDefinition = "BINARY(16)")
    private UUID courseId;

    @Column(name = "name")
    private String name;

    @Column(name = "password")
    private String password;

    // Constructors
    public Course() {
    }

    public Course(UUID courseId, String name, String password) {
        this.courseId = courseId;
        this.name = name;
        this.password = password;
    }

    public Course(String name, String password) {
        this.name = name;
        this.password = password;
    }

    // Getters and Setters
    public UUID getCourseId() {
        return courseId;
    }

    public void setCourseId(UUID course_id) {
        this.courseId = course_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}