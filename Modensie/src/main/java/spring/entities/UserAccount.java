package spring.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name="User_Account")
public class UserAccount {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "user_account_id", columnDefinition = "BINARY(16)")
    private UUID userAccountId;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "name")
    private String name;

    @JsonFormat(pattern="dd-MM-yyyy")
    @Column(name = "birthdate")
    private Date birthdate;

    @Column(name = "gender", nullable = true)
    private String gender;

    @Column(name = "email_address")
    private String emailAddress;

    @Column(name = "role")
    private String role;

    // Constructors
    public UserAccount() { }

    public UserAccount(UUID userAccountId, String username, String password, String name, Date birthdate, String gender, String emailAddress, String role) {
        this.userAccountId = userAccountId;
        this.username = username;
        this.password = password;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.emailAddress = emailAddress;
        this.role = role;
    }

    public UserAccount(String username, String password, String name, Date birthdate, String gender, String emailAddress, String role) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.emailAddress = emailAddress;
        this.role = role;
    }

    // Getters and Setters
    public UUID getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(UUID userAccountId) {
        this.userAccountId = userAccountId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String adress) {
        this.emailAddress = adress;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}