package spring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.dto.ParticipantCourseDTO;
import spring.entities.Course;
import spring.entities.ParticipantCourse;
import spring.entities.UserAccount;
import spring.repositories.CourseRepository;
import spring.repositories.ParticipantCourseRepository;
import spring.repositories.UserAccountRepository;

import java.util.*;

@Service
public class ParticipantCourseService {

    @Autowired
    private ParticipantCourseRepository participantCourseRepository;
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private UserAccountRepository userAccountRepository;

    // ADD
    public void addParticipantCourse(ParticipantCourseDTO participantCourse){
        UserAccount user = userAccountRepository.findByUsername(participantCourse.getUsernameUser());
        Course course =  courseRepository.findByCourseName(participantCourse.getCourseName());

        ParticipantCourse newParticipantCourse = new ParticipantCourse(course, user);
        participantCourseRepository.save(newParticipantCourse);
    }

    // FIND
    public List<ParticipantCourse> findAllParticipantCourses() {
        List<ParticipantCourse> participantCourses = participantCourseRepository.findAll();
        return participantCourses;
    }

    public List<String> findAllCoursesOfUser(String username)
    {
        List<String> participantCourses = participantCourseRepository.findAllCoursesOfUser(username);
        return participantCourses;
    }

    public List<String> findAllCoursesNotFollowedByUser(String username)
    {
        List<String> participantCourses = participantCourseRepository.findAllCoursesNotFollowedByUser(username);
        return participantCourses;
    }

    public List<UserAccount> findAllParticipantsOfCourse(String courseName){
        List<Object[]> userAccounts = participantCourseRepository.findAllParticipantsOfCourse(courseName);

        ArrayList<UserAccount> newList = new ArrayList<UserAccount>();
        for(Object[] userAccount: userAccounts) {
            newList.add(new UserAccount(userAccount[1].toString(), userAccount[2].toString(), userAccount[3].toString(), (Date) userAccount[4], userAccount[5].toString(), userAccount[6].toString(), userAccount[7].toString()));
        }
        return newList;
    }

    public List<String> findAllStudentsOfCourse(String courseName) {
        List<String> courses = participantCourseRepository.findAllStudentsOfCourse(courseName);
        return courses;
    }

    public List<String> findAllTeachersOfCourse(String courseName) {
        List<String> courses = participantCourseRepository.findAllTeachersOfCourse(courseName);
        return courses;
    }

    public ParticipantCourse checkUserAssignedToCourse(String username, String courseName) {
        ParticipantCourse participantCourse = participantCourseRepository.checkUserAssignedToCourse(username, courseName);
        return participantCourse;
    }

    // DELETE
    public void deleteParticipantCourse(String username, String courseName)
    {
        ParticipantCourse participantCourse = checkUserAssignedToCourse(username, courseName);
        participantCourseRepository.delete(participantCourse);
    }
}
