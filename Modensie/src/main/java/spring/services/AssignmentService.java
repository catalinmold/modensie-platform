package spring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.dto.AssignmentCourseDTO;
import spring.dto.AssignmentDTO;
import spring.entities.Assignment;
import spring.entities.Course;
import spring.entities.UserAccount;
import spring.repositories.AssignmentRepository;
import spring.repositories.CourseRepository;
import spring.repositories.UserAccountRepository;

import java.util.*;

@Service
public class AssignmentService {

    @Autowired
    private AssignmentRepository assignmentRepository;
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private UserAccountRepository userAccountRepository;

    // ADD
    public void addAssignment(AssignmentDTO assignment){
        UserAccount user = userAccountRepository.findByUsername(assignment.getTeacher());
        Course course =  courseRepository.findByCourseName(assignment.getCourseName());

        Assignment newAssignment= new Assignment(course, user, assignment.getName(), assignment.getDate(), assignment.getType(), assignment.getDescription());
        assignmentRepository.save(newAssignment);
    }

    // FIND
    public List<AssignmentDTO> findAllAssigmentOfCourse(String courseName) {
        List<Object[]> assignments = assignmentRepository.findAllAssigmentOfCourse(courseName);

        ArrayList<AssignmentDTO> newList = new ArrayList<AssignmentDTO>();
        for(Object[] assignment: assignments) {
            newList.add(new AssignmentDTO(assignment[0].toString(), assignment[1].toString(), (Date) assignment[2], (Integer) assignment[3], assignment[4].toString()));
        }
        return newList;
    }

    public Assignment checkAssignmentAssignedToCourse(String assignmentName, String courseName) {
        Assignment assignment = assignmentRepository.checkAssignmentAssignedToCourse(assignmentName, courseName);
        return assignment;
    }

    // EDIT (DTO used)
    public void editAssignment(AssignmentCourseDTO assignmentCourse) {
        Assignment assignment = assignmentRepository.findByAssignmentName(assignmentCourse.getAssignmentName(),assignmentCourse.getCourseName());
        assignment.setDescription(assignmentCourse.getDescription());
        assignmentRepository.save(assignment);
    }

    // DELETE
    public void deleteAssignment(String assignmentName, String courseName)
    {
        Assignment assignment = checkAssignmentAssignedToCourse(assignmentName, courseName);
        assignmentRepository.delete(assignment);
    }
}
