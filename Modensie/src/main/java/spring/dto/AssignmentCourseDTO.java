package spring.dto;

public class AssignmentCourseDTO {

    private String assignmentName;
    private String courseName;
    private String description;

    // Constructors
    public AssignmentCourseDTO() {
    }

    public AssignmentCourseDTO(String assignmentName, String courseName, String description) {
        this.assignmentName = assignmentName;
        this.courseName = courseName;
        this.description = description;
    }

    // Getters and Setters
    public String getAssignmentName() {
        return assignmentName;
    }

    public void setAssignmentName(String assignmentName) {
        this.assignmentName = assignmentName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
