package spring.dto;

import java.util.Date;

public class AssignmentDTO
{
    private String courseName;
    private String name;
    private String teacher;
    private Date date;
    private Integer type;
    private String description;

    // Constructors
    public AssignmentDTO() { }

    public AssignmentDTO(String name, String teacher, Date date, Integer type, String description) {
        this.name = name;
        this.teacher = teacher;
        this.date = date;
        this.type = type;
        this.description = description;
    }

    public AssignmentDTO(String courseName, String name, String teacher, Date date, Integer type, String description) {
        this.name = name;
        this.teacher = teacher;
        this.date = date;
        this.type = type;
        this.description = description;
    }

    // Getters and Setters
    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
