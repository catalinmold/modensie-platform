package spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.entities.Course;
import spring.services.CourseService;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200") // CORS policy: No 'Access-Control-Allow-Origin'
@RestController
@RequestMapping("/courses") // http://localhost:8080(/courses)
public class CourseController {

    @Autowired
    CourseService courseService;

    //@GetMapping
    //@PostMapping
    //@PutMapping
    //@DeleteMapping

    //---------------- (GET Requests)

    // Find All Courses
    @GetMapping(value={"allCourses"})// (/courses)/allCourses
    private List<Course> getAllCourses(){
        return courseService.findAllCourses();
    }

    // GET All Courses Name
    @GetMapping(value={"allCoursesName"})
    private List<String> getAllCoursesName(){
        return courseService.getAllCoursesName();
    }

    // Find By Course Name
    @GetMapping(value={"checkCourse"})
    private Course checkCourse(@RequestParam("courseName") String courseName){
        return courseService.checkCourse(courseName);
    }

    //---------------- (PUT Requests)

    // UPDATE Course Name and Password
    @PutMapping (value = { "/put/editCoursePassword" })
    private void editCourse (@RequestBody Course password) { courseService.editCoursePassword(password);
    }

    //---------------- (POST Requests)

    // INSERT Course
    @PostMapping(value = { "/post/insertCourse" })
    private void registerCourse(@RequestBody Course course) {
        courseService.addCourse(course);
    }

    //---------------- (DELETE Requests)

    // DELETE Course
    @DeleteMapping(value = { "/delete/deleteCourse" })
    private void deleteCourse(@RequestParam("courseName") String courseName) {
        courseService.deleteCourse(courseName);
    }
}
