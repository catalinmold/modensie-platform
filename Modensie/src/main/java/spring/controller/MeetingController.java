package spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.dto.MeetingDTO;
import spring.entities.Meeting;
import spring.services.MeetingService;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200") // CORS policy: No 'Access-Control-Allow-Origin'
@RestController
@RequestMapping("/meeting") // http://localhost:8080(/meeting)
public class MeetingController {

    @Autowired
    MeetingService meetingService;

    //@GetMapping
    //@PostMapping
    //@PutMapping
    //@DeleteMapping

    //---------------- (GET Requests)

    // Find All Assignments of Course
    @GetMapping(value={"allMeetingsOfUser"})// (/meeting)/allMeetingsOfUser
    private List<MeetingDTO> getAllMeetingsOfUser(@RequestParam("username") String username){
        return meetingService.findAllMeetingsOfUser(username);
    }
    /*
    //---------------- (POST Requests)

    // INSERT Assignment
    @PostMapping(value = { "/post/insertAssignment" })
    private void addAssignment(@RequestBody AssignmentDTO assignment) {
        assignmentService.addAssignment(assignment);
    }

    // UPDATE Assignment
    @PutMapping (value = { "/put/editAssignment" })
    private void editAssignment (@RequestBody AssignmentCourseDTO assignmentCourse) {
        assignmentService.editAssignment(assignmentCourse);
    }

    //---------------- (DELETE Requests)

    // DELETE Assignment
    @DeleteMapping(value = { "/delete/deleteAssignmentCourse" })
    private void deleteParticipantCourse(@RequestParam("assignmentName") String assignmentName, @RequestParam("courseName") String courseName) {
        assignmentService.deleteAssignment(assignmentName, courseName);
    }

     */
}
