package spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import spring.entities.Assignment;

import java.util.List;

@Repository
public interface AssignmentRepository extends JpaRepository<Assignment,Integer> {

    @Query(value="SELECT Assignment.name, User_Account.name AS teacher, date, type, description FROM Assignment " +
            "JOIN Course ON Course.course_id = Assignment.course_id " +
            "JOIN User_Account ON user_account_id = Assignment.user_id " +
            "WHERE Course.name = (?1)", nativeQuery = true)
    public List<Object[]> findAllAssigmentOfCourse(String courseName);

    @Query(value="SELECT Assignment.* FROM Assignment " +
            "JOIN Course ON Course.course_id = Assignment.course_id " +
            "WHERE Assignment.name = (?1) AND Course.name = (?2)", nativeQuery = true)
    public Assignment checkAssignmentAssignedToCourse(String assignmentName, String courseName);

    @Query(value="SELECT Assignment.* FROM Assignment JOIN Course ON Course.course_id = Assignment.course_id " +
            "WHERE Assignment.name = (?1) AND Course.name = (?2)", nativeQuery = true)
    public Assignment findByAssignmentName(String assignmentName, String courseName);
}
