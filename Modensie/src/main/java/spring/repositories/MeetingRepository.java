package spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import spring.dto.MeetingDTO;
import spring.entities.Meeting;

import java.util.List;

@Repository
public interface MeetingRepository extends JpaRepository<Meeting,Integer> {

    @Query(value="SELECT Course.name AS courseName, Meeting.*  FROM Meeting JOIN Course ON Meeting.course_id = Course.course_id " +
            "JOIN Participant_Course ON Course.course_id = Participant_Course.course_id " +
            "JOIN User_Account ON User_Account.user_account_id = Participant_Course.user_id " +
            "    WHERE User_Account.username = (?1)", nativeQuery = true)
    public List<Object[]> findAllMeetingsOfUser(String username);
}
