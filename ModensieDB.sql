/**********************************************
 *  Modensie                                  *
 *  École Normale Supérieure de Lyon          *
 *  Département Informatique - M1             *
 **********************************************/

-- --------------------------------------------------------
-- Create Database
-- --------------------------------------------------------
-- DROP DATABASE IF EXISTS ModensiePlatformDB;
CREATE DATABASE IF NOT EXISTS ModensiePlatformDB;
USE ModensiePlatformDB;

-- --------------------------------------------------------
-- User Privileges
-- --------------------------------------------------------
-- DROP USER IF EXISTS 'modensie_user';
-- CREATE USER 'modensie_user'@'%' IDENTIFIED BY 'ENSpassM123';
-- GRANT ALL ON ModensiePlatformDB.* TO 'modensie_user'@'%';
-- FLUSH PRIVILEGES;
-- SHOW GRANTS FOR 'modensie_user'@localhost;

-- Refresh Tables
DROP TABLE IF EXISTS Meeting;
DROP TABLE IF EXISTS Assignment;
DROP TABLE IF EXISTS Participant_Course;
DROP TABLE IF EXISTS Course;
DROP TABLE IF EXISTS User_Account;

-- -----------------------------------------------------
-- Tables Creation
-- -----------------------------------------------------
-- Table User_Account
DROP TABLE IF EXISTS User_Account;
CREATE TABLE User_Account(
user_account_id binary(16) UNIQUE PRIMARY KEY,
username varchar(64) UNIQUE NOT NULL,
password varchar(64) NOT NULL,
name varchar(64) NOT NULL,
birthdate date NOT NULL,
gender varchar(1),
email_address varchar(64) NOT NULL,
role varchar(64) NOT NULL
);

-- Table Course
DROP TABLE IF EXISTS Course;
CREATE TABLE Course(
course_id binary(16) UNIQUE PRIMARY KEY,
name varchar(64) UNIQUE NOT NULL,
password varchar(64)
);

-- Table Participant_Course
DROP TABLE IF EXISTS Participant_Course;
CREATE TABLE Participant_Course(
participant_course_id binary(16) UNIQUE PRIMARY KEY,
course_id binary(16) NOT NULL,
user_id binary(16) NOT NULL
);

-- Table Assignment
DROP TABLE IF EXISTS Assignment;
CREATE TABLE Assignment(
assignment_id binary(16) UNIQUE PRIMARY KEY,
course_id binary(16) NOT NULL,
user_id binary(16) NOT NULL,
name varchar(64) NOT NULL,
date datetime NOT NULL DEFAULT now(),
type int(1) NOT NULL,	-- 0(lesson)/1(exercise)/2(news)
description varchar(256)
);

-- Table Meeting
DROP TABLE IF EXISTS Meeting;
CREATE TABLE Meeting(
meeting_id binary(16) UNIQUE PRIMARY KEY,
course_id binary(16) NOT NULL,
name varchar(64) NOT NULL,
room varchar(64),
start_time datetime NOT NULL,
end_time datetime NOT NULL,
description varchar(256),
link varchar(64)
);

-- -----------------------------------------------------
-- FOREIGN KEYS
-- -----------------------------------------------------
ALTER TABLE Participant_Course 
 	ADD FOREIGN KEY (course_id) REFERENCES Course(course_id),
 	ADD FOREIGN KEY (user_id) REFERENCES User_Account(user_account_id);
ALTER TABLE Assignment 
 	ADD FOREIGN KEY (course_id) REFERENCES Course(course_id),
 	ADD FOREIGN KEY (user_id) REFERENCES User_Account(user_account_id);
ALTER TABLE Meeting 
 	ADD FOREIGN KEY (course_id) REFERENCES Course(course_id);

-- -----------------------------------------------------
-- CONSTRAINTS
-- -----------------------------------------------------
/* Referential integrity constraints */
ALTER TABLE Participant_Course ADD CONSTRAINT Participant_Course_Constraint
FOREIGN KEY (course_id) REFERENCES Course(course_id) 
ON DELETE CASCADE;

ALTER TABLE Assignment ADD CONSTRAINT Assignment_Constraint
FOREIGN KEY (course_id) REFERENCES Course(course_id) 
ON DELETE CASCADE;

ALTER TABLE Meeting ADD CONSTRAINT Meeting_Constraint
FOREIGN KEY (course_id) REFERENCES Course(course_id) 
ON DELETE CASCADE;

/* Domain constraints */
ALTER TABLE User_Account ADD CONSTRAINT UserAccount_Constraint_Gender
CHECK(gender = 'M' OR gender = 'F');

ALTER TABLE User_Account ADD CONSTRAINT UserAccount_Constraint_Email
CHECK(email_address LIKE '%_@__%.__%');

-- -----------------------------------------------------
-- Triggers
-- -----------------------------------------------------
DELIMITER #
CREATE TRIGGER GenerateUUID_User_Account BEFORE INSERT ON User_Account
FOR EACH  ROW 
BEGIN 
    SET NEW.user_account_id = UUID(); 
END;
#
DELIMITER ;

DELIMITER #
CREATE TRIGGER GenerateUUID_Course BEFORE INSERT ON Course
FOR EACH  ROW 
BEGIN 
    SET NEW.course_id = UUID(); 
END;
#
DELIMITER ;

DELIMITER #
CREATE TRIGGER GenerateUUID_Participant_Course BEFORE INSERT ON Participant_Course
FOR EACH  ROW 
BEGIN 
    SET NEW.participant_course_id = UUID(); 
END;
#
DELIMITER ;

DELIMITER #
CREATE TRIGGER GenerateUUID_Assignment BEFORE INSERT ON Assignment
FOR EACH  ROW 
BEGIN 
    SET NEW.assignment_id = UUID(); 
END;
#
DELIMITER ;

DELIMITER #
CREATE TRIGGER GenerateUUID_Meeting BEFORE INSERT ON Meeting
FOR EACH  ROW 
BEGIN 
    SET NEW.meeting_id = UUID(); 
END;
#
DELIMITER ;

-- -----------------------------------------------------
-- Registrations
-- -----------------------------------------------------
INSERT INTO User_Account(username,password,name,birthdate,gender,email_address,role)
VALUES
('melanie_laurent', 'qwerty123', 'Mélanie Laurent', '1995-7-04', 'F', 'melanie.laurent@ens-lyon.fr', 'student'),
('emman_beart', 'hf367g3d', 'Emmanuelle Béart', '1996-11-15', 'M', 'emmanuelle.beart@ens-lyon.fr', 'student'),
('bernblier', '34jtv45ug8', 'Bernard Blier', '1996-6-14', 'M', 'bernard.blier@ens-lyon.fr', 'student'),
('laet_casta', '0-o5op4ky59', 'Laetitia Casta', '2001-3-01', 'F', 'laetitia.casta@ens-lyon.fr', 'student'),
('irene_jacob', 'oi4893jf', 'Irène Jacob', '1994-3-14', 'F', 'irene.jacob@ens-lyon.fr', 'student'),
('jean_dujardin', '43f34f3e', 'Jean Dujardin', '1996-6-24', 'M', 'jean.dujardin@ens-lyon.fr', 'student'),
('louis_funes', 'lturb894my', 'Louis de Funès', '1994-3-18', NULL, 'louis.funes@ens-lyon.fr', 'student'),
('alexia_portal', '34ergv37', 'Alexia Portal', '2003-12-10', 'F', 'alexia.portal@ens-lyon.fr', 'student'),
('yvonprint2', 'gj5468h5', 'Yvonne Printemps', '2000-4-03', 'F', 'yvonne.printemps@ens-lyon.fr', 'student'),
('herve_villec', '123456', 'Hervé Villechaize', '1974-1-09', 'M', 'hervé.villechaize@ens-lyon.fr', 'teacher'),
('audrey_tautou', 'regvdfgh54', 'Audrey Tautou', '1956-2-19', 'F', 'audrey.tautou@ens-lyon.fr', 'teacher'),
('olivier_martinez', '34v645yh', 'Olivier Martinez', '1962-2-22', 'F', 'olivier.martinez@ens-lyon.fr', 'teacher');

INSERT INTO Course(name,password)
VALUES
('Computer Algebra', 'welcometoCA2021'),
('Cryptography and Security', 'welcometoCS2021'),
('Data Bases and Data Mining', 'welcometoDBDM2021'),
('Programs and Proofs', NULL);

INSERT INTO Participant_Course(course_id,user_id)
VALUES
((SELECT course_id FROM Course WHERE name = 'Computer Algebra'),(SELECT user_account_id FROM User_Account WHERE username = 'herve_villec')),
((SELECT course_id FROM Course WHERE name = 'Computer Algebra'),(SELECT user_account_id FROM User_Account WHERE username = 'audrey_tautou')),
((SELECT course_id FROM Course WHERE name = 'Computer Algebra'),(SELECT user_account_id FROM User_Account WHERE username = 'melanie_laurent')),
((SELECT course_id FROM Course WHERE name = 'Computer Algebra'),(SELECT user_account_id FROM User_Account WHERE username = 'emman_beart')),
((SELECT course_id FROM Course WHERE name = 'Computer Algebra'),(SELECT user_account_id FROM User_Account WHERE username = 'laet_casta')),
((SELECT course_id FROM Course WHERE name = 'Cryptography and Security'),(SELECT user_account_id FROM User_Account WHERE username = 'herve_villec')),
((SELECT course_id FROM Course WHERE name = 'Cryptography and Security'),(SELECT user_account_id FROM User_Account WHERE username = 'melanie_laurent')),
((SELECT course_id FROM Course WHERE name = 'Data Bases and Data Mining'),(SELECT user_account_id FROM User_Account WHERE username = 'audrey_tautou')),
((SELECT course_id FROM Course WHERE name = 'Programs and Proofs'),(SELECT user_account_id FROM User_Account WHERE username = 'olivier_martinez'));

INSERT INTO Assignment(course_id,user_id,name,type,description,date)
VALUES
((SELECT course_id FROM Course WHERE name = 'Computer Algebra'),(SELECT user_account_id FROM User_Account WHERE username = 'herve_villec'),'Numbers',0,'Numbers, variables and mathematical expressios',NOW()),
((SELECT course_id FROM Course WHERE name = 'Computer Algebra'),(SELECT user_account_id FROM User_Account WHERE username = 'audrey_tautou'),'Simplification',0,'Basic rules of differentiation',SUBDATE(NOW(),1)),
((SELECT course_id FROM Course WHERE name = 'Computer Algebra'),(SELECT user_account_id FROM User_Account WHERE username = 'herve_villec'),'Equality',1,'Rational fractions exercises',SUBDATE(NOW(),2)),
((SELECT course_id FROM Course WHERE name = 'Cryptography and Security'),(SELECT user_account_id FROM User_Account WHERE username = 'melanie_laurent'),'Classic cryptography',2,'The main classical ciphers (transposition ciphers)',NOW()),
((SELECT course_id FROM Course WHERE name = 'Programs and Proofs'),(SELECT user_account_id FROM User_Account WHERE username = 'olivier_martinez'),'Programming language theory',1,'The Curry–Howard correspondence',SUBDATE(NOW(),1)),
((SELECT course_id FROM Course WHERE name = 'Programs and Proofs'),(SELECT user_account_id FROM User_Account WHERE username = 'olivier_martinez'),'Origin, scope, and consequences',0,'he beginnings of the Curry–Howard correspondence',NOW());

INSERT INTO Meeting(course_id,name,room,start_time,end_time,description,link)
VALUES
((SELECT course_id FROM Course WHERE name = 'Computer Algebra'),'Numbers and Simplifications Part 2','BBB Online',DATE_SUB(NOW(), INTERVAL 3 HOUR),NOW(),'Basic computations','https://www.bbb.org/computer_algebra'),
((SELECT course_id FROM Course WHERE name = 'Computer Algebra'),'Numbers and Simplifications Part 1','BBB Online',SUBDATE(DATE_SUB(NOW(), INTERVAL 5 HOUR),1),SUBDATE(DATE_SUB(NOW(), INTERVAL 2 HOUR),1),'Basic computations','https://www.bbb.org/computer_algebra'),
((SELECT course_id FROM Course WHERE name = 'Cryptography and Security'),'Cryptography Q&A','BBB Online',SUBDATE(DATE_SUB(NOW(), INTERVAL 7 HOUR),2),SUBDATE(DATE_SUB(NOW(), INTERVAL 4 HOUR),2),'Ciphers and other stuff','https://www.bbb.org/cryptography'),
((SELECT course_id FROM Course WHERE name = 'Programs and Proofs'),'Online Exercises','BBB Online',SUBDATE(DATE_SUB(NOW(), INTERVAL 3 HOUR),1),SUBDATE(NOW(),1),'Easy and Complex Exercises','https://www.bbb.org/programs_and_proofs'),
((SELECT course_id FROM Course WHERE name = 'Programs and Proofs'),'Online Exercises','BBB Online',SUBDATE(DATE_SUB(NOW(), INTERVAL 12 HOUR),3),SUBDATE(DATE_SUB(NOW(), INTERVAL 9 HOUR),3),'Easy and Complex Exercises','https://www.bbb.org/programs_and_proofs');
