import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataRequestService} from '../core/data.service';
import {UserAccount, UserAccountEmailAddress, UserAccountPassword} from '../core/data.model';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  myUser: UserAccount;
  tabRole: string;
  linkRole: string;

  userPassword: UserAccountPassword = new UserAccountPassword();
  oldPassword: string;
  errorMessage: string;
  userEmailAddress: UserAccountEmailAddress = new UserAccountEmailAddress();

  constructor(private router: Router, private dataRequestService: DataRequestService, private cookieService: CookieService) { }

  ngOnInit(): void {
    if (!this.cookieService.check('userLogged')) {
      this.router.navigate(['login_page']);
    }

    this.dataRequestService.getUserAccountDetails(this.cookieService.get('userLogged'))
      .subscribe(data => {
        this.myUser = data;
      });

    this.tabRole = 'Courses';
    if (this.cookieService.get('userRole') === 'student') {
      this.linkRole = 'student_page';
    } else if (this.cookieService.get('userRole') === 'teacher') {
      this.linkRole = 'teacher_page';
    }
  }

  changePassword(): void {
    this.userPassword.username = this.myUser.username;
    if(this.oldPassword === this.myUser.password) {
      if(this.userPassword.password !== undefined) {
        if (this.userPassword.password !== undefined) {
          this.dataRequestService.editUserAccountPassword(this.userPassword)
            .subscribe(data => this.errorMessage = 'Password changed');
        }
      } else {
        this.errorMessage = 'Invalid Password';
      }
    } else {
      this.errorMessage = 'Your actual password is invalid';
    }
  }

  changeEmailAddress() {
    this.userEmailAddress.username = this.myUser.username;
    if (this.userEmailAddress.emailAddress !== undefined) {
      this.dataRequestService.editUserAccountEmailAddress(this.userEmailAddress)
        .subscribe(data => location.reload());
    }
  }

  showField(fieldName: string): void {
    if (fieldName === 'passwordField') {
      if ((document.querySelector('.password-field') as HTMLElement).style.display === 'none')
        (document.querySelector('.password-field') as HTMLElement).style.display = 'block';
      else
        (document.querySelector('.password-field') as HTMLElement).style.display = 'none';
    }
    else
    if (fieldName === 'emailAddressField') {
      if ((document.querySelector('.email-address-field') as HTMLElement).style.display === 'none')
        (document.querySelector('.email-address-field') as HTMLElement).style.display = 'block';
      else
        (document.querySelector('.email-address-field') as HTMLElement).style.display = 'none';
    }
  }

  logout(): void {
    this.cookieService.delete('userLogged');
    this.cookieService.delete('userRole');
  }

}
