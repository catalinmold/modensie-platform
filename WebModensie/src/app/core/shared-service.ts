import {BehaviorSubject} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable()
export class SharedService {
  private myUsername: string;
  private myRole: string;

  getMyRole(): string {
    return this.myRole;
  }

  setMyRole(value: string) {
    this.myRole = value;
  }


  constructor() {
    this.myUsername = 'DEFAULT';
  }

  getValue(): string {
    return this.myUsername;
  }

  setValue(value: string) {
    this.myUsername = value;
  }
}
